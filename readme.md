# Laravel slack logging

This package is an extension to the slack logging withing laravel.
As it stands now it only adds the project name, with a link to the logging element in slack.

### Installation
To install you should require the composer package
```
composer require doitonlinemedia/laravel-slack-logging
```
After, you change the slack channel in ```logging.php``` to incororate the following
```
'tap' => [
    Doitonlinemedia\LaravelSlackLogging\SlackLogger::class,
],
```