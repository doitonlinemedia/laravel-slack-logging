<?php
namespace Doitonlinemedia\LaravelSlackLogging;

use Monolog\Handler\SlackWebhookHandler;

class SlackLogger
{
    /**
     * Customize the given logger instance.
     *
     * @param  \Illuminate\Log\Logger  $logger
     * @return void
     */
    public function __invoke($logger)
    {
        foreach ($logger->getHandlers() as $handler) {
            if ($handler instanceof SlackWebhookHandler) {
                
                $handler->pushProcessor(function ($record) use ($handler) {
                    $record['extra']['Project'] = '<' . config( 'app.url') . '|' . config('app.name') . '>';
                    
                    return $record;
                });
                
            }
        }
    }

}